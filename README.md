# Workstation

<img src="./robot.png" alt="drawing" width="250"/>

Esse repositório contribui no estudo do Ansible e aproveita para automatizar uma workstation de trabalho.

Motivos para preparar workstation completo no ansible.

- Velocidado no caso de ter que formatar a máquina
- Aprendizado de mais recursos do Ansible
- Ambientes idênticos para o time caso necessário

>`Tanto o Arch Linux quanto no Uuntu possuem configurações para o ambiente gráfico Gnome. Caso esteja utilizando algum ambiente diferente comente a role desktop em main.yaml no ambiente que escolher.`

As roles foram dividas da seguinte forma.

- base: Cuidará dos drivers e da configuração do gerenciador de pacote caso necesário, bem como atualização do sistema.
- install: Instalará os pacotes básicos e software que geralmente usamos.
  - resources: Instalação de pacotes básicos do sistema, geralmente necessário por outras ferramentas.
  - software: Instalação dos softwares de uso normal
- customize: Essa etapa irá fazer toda a customização de tools.
- desktop: Customização do desktop gnome, themas, extensões, icones, fontes, etc
- techs: Instalação de todo ferramental necessário para trabalhar com TI. Depende da área de atuação. Nessa proposta gosto muito de utilizar como gerenciador dessas ferramentas o [asdf](https://github.com/asdf-vm/asdf).
- playground: Etapa de instalação de vms, docker, cluster, etc. Essa etapa só tem sentido se você usa esses recursos.

Aquilo que não tiver valia no seu ambiente, comente no código.

Na pasta raiz temos o main.yam chamar todas as roles, comente por exemplo a role que não quiser usar.

```bash
  roles:
    - role: prepare
      tags: prepare_role
    - role: install
      tags: install_role
    - role: customize
      tags: customize_role
    # - role: desktop
    #   tags: desktop_role
    - role: techs
      tags: techs_role
    # - role: playground
    #   tags: playground_role
```

Em relação a role `desktop`, esta em a minha personalização inicial. Um outro projeto Ansible é aplicado depois desse para deixar tudo ainda mais preparado. A instalação da extensão sync extensions depois sincronizará todas as extensões que uso.

## Arch Linux

Depois de instalado o sistema operacional Arch Linux entre no terminal e faça os seguintes comandos

```bash
sudo pacman -Syu # Para atualizar os repositórios padrão
sudo pacman -S install git curl ansible -y # Instale esses pacotes
ansible-galaxy collection install community.general # Instala collections que o playbook irá usar
ansible-galaxy install jahrik.yay
ansible-galaxy collection install kewlfft.aur
cd /tmp
git clone https://gitlab.com/davidpuziol/workstation.git
cd workstation/arch
ansible-playbook main.yaml --ask-become-pass # e digite a senha do seu usuáro
```

## Ubuntu

Depois de instalado o sistema operacional entre no terminal e faça os seguintes comandos

```bash
sudo apt-get update # Para atualizar os repositórios padrão
sudo apt-get install git curl ansible # Instale esses pacotes
ansible-galaxy collection install community.general # Instala collections que o playbook irá usar
cd /tmp
git clone https://gitlab.com/davidpuziol/workstation.git
cd workstation/ubuntu
ansible-playbook main.yaml --ask-become-pass # e digite a senha do seu usuáro
```

Novas idéias são sempre bem vinda caso queira colaborar!
