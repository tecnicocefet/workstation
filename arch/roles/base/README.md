Role Name
=========

base

Description
------------

prepara o sistema com um upgrade, instala drivers e o AUR yay

Requirements
------------

ansible-galaxy install jahrik.yay
ansible-galaxy collection install kewlfft.aur

Role Variables
--------------

Nenhuma varirável é necessária

Dependencies
------------

Não possui dependência.


License
-------

MIT